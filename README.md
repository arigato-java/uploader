# ホームページ更新ツール

ホームページをstatic filesとしてGit レポジトリで管理していて、公開はAWS S3互換のストレージとCDN という構成用のウェブサイト更新ツールです。

Git ログから、更新のあったファイルだけをS3互換のストレージにアップロードします。
今はAPI Quotaの兼ね合いで固定の4並列で処理しています。
更新があったURL はCDNのキャッシュ無効化を実施します。
アップロード成功すると、成功したコミットID をDynamoDB に書いておきます。
次回アップロード時はこのコミットID との差分をアップロードします。

コミットログからRSSとサイトマップを作って更新します。

ファイル名の正規表現マッチから、 `content-type`, `cache-control` の設定に加え、圧縮可否を指定できます。
圧縮可の設定を入れると、アップロード時に Gzip 互換の高圧縮率アルゴリズムである zopfli で圧縮を試み、元のファイルサイズより小さくなった場合は圧縮された状態で S3 に゙配置します。
この際、`content-encoding` を指定するので、ビューアーからすると送出時に圧縮するHTTP サーバと同じように見えますが、 `Accept-encoding` を見てないのがS3と普通のhttpサーバの挙動で違うところです。
とはいえ、すべてのブラウザが `gzip` に対応しているので、実際の利用のうえでは問題になりません。

CI 用の仕組みで動かすと、push だけで更新という感じになり非常に便利です。

## コンテナイメージ

自動でビルドされたコンテナイメージが `registry.gitlab.com/arigato-java/uploader:latest` として利用可能です。
[タグ一覧](https://gitlab.com/arigato-java/uploader/container_registry/6248132)

## 動作実績があるもの

### レポジトリ

- GitLab
  - GitLab とAWS はOIDC で連携することで永続的クレデンシャルを利用しなくて済む。
- Git (含Google Cloud Source Repositories)
- AWS CodeCommit
  - 上記どこまでアップロードしたかの管理もこのプログラム内で完結して便利です。

### オブジェクトストレージ

- Amazon S3
- Cloudflare R2
- Google Cloud Storage
  - これはS3互換エンドポイントを指定することで利用可能

### CDN

- Amazon CloudFront
- Cloudflare

### 通知

- Slack
- Discord
  - Slack互換エンドポイントを利用

### 検索エンジン

- BingのサイトマップPOST機能

### 実行環境

- AWS CodeBuild
  - aarch64 とx86_64 で実績あり
- Google Cloud Build
- GitLab CI

## 設定ファイル

`-c` オプションで指定する設定ファイルの例です。

```
metadata_rules: # ファイル名のパターンからcontent-type, cache-control, 圧縮要否を指定。それぞれ上から見ていって最初にマッチした指定が使われる
  - rule: '\.html$'
    content_type: text/html;charset=utf-8
  - rule: '\.[jJ][pP][eE]?[gG]$'
    content_type: image/jpeg
    cache_control: public,max-age=31536000
    compress: false
  - rule: '^+*$'
    cache_control: public,max-age=86700
    compress: true
ignore_path: # 更新差分判定時に無視したいファイル名のパターン
  - '^.gitignore'
sitemap:
  ignore_path: # sitemapに掲載しないファイル名。sitemap掲載はignore_pathとの和集合で判定してる。
    - '^google[0-9a-f]+\.html$'
  content_type: "application/xml"
  cache_control: "max-age=86400"
  path: sitemap.xml
rss_options: # RSSのオプション
  title: てすとぺーじ
  description: 説明です。
  language: ja
  image_file: icon.png
  upload_options:
    content_type: "application/rss+xml"
    cache_control: "max-age=86400"
  bake_html:
    item_count: 5 # html に焼き込むRSSの最大の項目数
    html_element_id: news # トップのul要素に指定する id属性
    target_html: index.html # 書換対象HTMLパス
    html_options:
      content_type: "text/html;charset=utf-8"
      cache_control: "max-age=86400"
  path: rss.xml
  open_search: # OpenSearch Protocol へのlinkをRSSに埋めるオプション https://github.com/dewitt/opensearch/
    href: "https://example.com/opensearch.xml"
    title: ホムペ全文検索
repository_name: super-repo # これを指定するとCodeCommitのアップロード済みブランチを自動で更新します。
environments:
  staging: # 環境名。好きな名前で。
    url: https://www-staging.example.com/
    buckets: # S3 バケット指定。複数指定すると同じ内容を全てに書きます（可用性対策）
      - name: www-staging1-example-com
        region: us-west-2
      - name: www-staging2-example-com
        region: us-east-2
    distribution_id: XXXXXX # CloudFrontの distribution id
    dynamodb_key: website-staging # どこまでアップロードしたか取っておくDynamoDBのキー名
  prod:
    url: https://www.example.com/
    buckets:
      - name: example.com
        region: us-east-1
        endpoint: https://storage.googleapis.com # Google Cloud Storageを使う例。
    bucket_aws_profile: gcpsigv4 # Bucket にアクセスする時のAWS Profile. S3互換サービス利用時DynamoDBとプロファイルを分けたいケースがあるため。
    cloudflare_zone: xxxxxxxxxx # Cloudflare Zone ID
    dynamodb_key: website-prod # どこまでアップロードしたか取っておくDynamoDBのキー名
dynamodb: # アップロード済みのコミットハッシュをとっておくためのDynamoDB テーブルを指定
  table: uploaded
  region: ap-northeast-3
```

## 環境変数

- *SLACK_WEBHOOK_URL* Slackに更新通知を送る先のURL
- *BING_SUBMIT_URL* Bingにサイトマップ送るときのURL。URLにトークンが含まれてる。
- *AWS_ACCESS_KEY_ID* *非推奨(OIDC利用がオススメ)* S3に゙アクセスする際などに使う認証情報。公開しないよう注意。
- *AWS_SECRET_ACCESS_KEY* *非推奨(OIDC利用がオススメ)* S3に゙アクセスする際などに使う認証情報。公開しないよう注意。
- *CLOUDFLARE_KEY* Cloudflare 使用時にキャッシュパージに使うためのAPIキー。

## コマンドラインオプション

オンラインヘルプが出るので見てください。

```
Usage: uploader [OPTIONS] --config <CONFIG> <COMMAND>

Commands:
  upload         Upload chaged files and notify external services
  copy           Put specified files to storage
  delete         Delete specified objects from storage
  rss            Generate RSS
  sitemap        Generate Sitemap
  invalidate     Invalidate CDN
  submit-url     Submit URL to search engine
  update-branch  Update uploaded git branch to point to HEAD
  slack          Test slack notification
  help           Print this message or the help of the given subcommand(s)

Options:
  -c, --config <CONFIG>  Path to yaml configuration file
  -e, --env <ENV>        Envronment (prod, staging) [default: staging]
  -h, --help             Print help
  -V, --version          Print version
```
## RSSのつくりかた

直近のコミットログから作りますが、コミットメッセージ冒頭が `[rss skip]` で始まるコミットはRSSのエントリにしません。

## 便利な使い方

### Amazon Web Services編

CodeCommitでレポジトリを用意し、EventBridge経由でmainブランチの場合だけCodeBuildのジョブをトリガしましょう。

### Google Cloud Platform編

Google Cloud Source Repositoriesを使い、Cloud Buildのトリガーに指定しましょう。

## ライセンス

Copyright 2024 Hisanobu Tomari

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
