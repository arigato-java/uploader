use build_html::{escape_html, Html, HtmlElement, HtmlTag};
use regex::Regex;
use rss::ChannelBuilder;
use serde::Deserialize;
use std::fs;
use std::sync::Arc;

use crate::*;

// Yaml の設定ファイル
#[derive(Deserialize)]
pub struct RssWriterOptions {
    /// rss にそのまま出力するtitle
    title: String,
    /// rss にそのまま出力する description
    description: String,
    /// rss の言語設定
    language: String,
    /// rssに書く画像ファイルのパス。パスはそのままrssに書き出します。
    image_file: String,
    /// rss の生成結果をアップロードする時の設定
    pub(crate) upload_options: RssUploadOptions,
    /// html焼き込みオプション
    pub(crate) bake_html: RssBakeParameters,
    /// rssのパス
    pub(crate) path: Arc<Path>,
    /// Open Search形式のXMLへのリンクををRSSに書き込むパラメータ
    open_search: Option<LinkOpenSearchParams>,
}
#[derive(Deserialize)]
pub struct RssUploadOptions {
    /// content-type
    pub(crate) content_type: String,
    /// cache-control
    pub(crate) cache_control: String,
}
#[derive(Deserialize)]
pub struct RssBakeParameters {
    /// htmlに焼き込む項目の数
    item_count: u64,
    /// html焼き込みの時のトップHTML要素のID
    html_element_id: String,
    /// 焼き込むhtmlのパス
    pub(crate) target_html: Arc<Path>,
    // htmlアップロード時の設定
    pub(crate) html_options: RssUploadOptions,
}
/// Atom LinkでOpen Search xmlにリンクを書くところのパラメータ
#[derive(Deserialize)]
struct LinkOpenSearchParams {
    /// XMLへのURL
    href: String,
    /// title
    title: Option<String>,
}

fn make_rss_item_from_commit(
    repo: &Repository,
    commit: &git2::Commit,
    ignore: &regex::RegexSet,
    baseurl: &String,
) -> Option<rss::Item> {
    let message = commit.summary().unwrap();
    if message.starts_with("[rss skip]") {
        return None;
    }

    let tree_new = &commit.tree().ok();
    let parent = commit.parent(0);
    if parent.is_err() {
        return None;
    }
    let tree_old = &parent.unwrap().tree().ok();

    // 日付
    let commit_datetime_local = get_commit_localtime(commit);

    let commit_id_new = commit.id();
    let item_guid = commit_id_new.to_string();

    let diff = repo
        .diff_tree_to_tree(tree_old.as_ref(), tree_new.as_ref(), None)
        .ok()?;

    let mut longest_path: std::option::Option<String> = None;
    let mut longest_len = 0;
    diff.foreach(
        &mut |delta: git2::DiffDelta, _val: f32| {
            if let Some(path) = delta.new_file().path() {
                if ignore.is_match(path.to_str().unwrap()) {
                    return true;
                }
                let status = delta.status();
                if status != git2::Delta::Added && status != git2::Delta::Modified {
                    return true;
                }
                // 決め打ちでいいか？
                if let Some(ext) = path.extension() {
                    if ext == "html" {
                        if let Some(path_str) = path.to_str() {
                            let mut this_len = path_str.len();
                            if path_str.ends_with("index.html") {
                                this_len -= 10;
                            }
                            if longest_path.is_none() || (longest_len < this_len) {
                                let path_string = String::from(path_str);
                                longest_len = this_len;
                                longest_path = Some(path_string);
                            }
                        }
                    }
                }
            }
            true
        },
        None,
        None,
        None,
    )
    .ok();
    match longest_path {
        Some(longest_path_unwrapped) => {
            let mut item = rss::Item::default();
            item.set_title(Some(String::from(message)));
            item.set_description(Some(String::from(message)));
            item.set_link(Some(make_url_for_path(baseurl, &longest_path_unwrapped)));
            item.set_pub_date(commit_datetime_local.to_rfc2822());
            item.set_guid(Some(rss::Guid {
                value: item_guid,
                permalink: false,
            }));
            Some(item)
        }
        None => None,
    }
}

/// RSSのアイテムをhtmlにbakeするためにHTML化する
fn bake_rss_to_html(config: &UploaderConfig, items: &[rss::Item]) -> String {
    let mut html_item_count = 0;
    let mut ul = HtmlElement::new(HtmlTag::UnorderedList)
        .with_attribute("id", &config.rss_options.bake_html.html_element_id);
    for entry in items.iter() {
        let pub_date =
            chrono::DateTime::parse_from_rfc2822(entry.pub_date.as_ref().unwrap()).unwrap();
        let pub_date_text = format!("{}", pub_date.format("%F"));
        let em = HtmlElement::new(HtmlTag::Span)
            .with_attribute("class", "rssdatetime")
            .with_child(escape_html(&pub_date_text).into());
        let a = HtmlElement::new(HtmlTag::Link)
            .with_attribute("href", escape_html(entry.link.as_ref().unwrap()))
            .with_child(escape_html(entry.description.as_ref().unwrap()).into());

        let li = HtmlElement::new(HtmlTag::ListElement)
            .with_child(em.into())
            .with_child(" ".into())
            .with_child(a.into());
        ul.add_child(li.into());
        html_item_count += 1;
        if html_item_count >= config.rss_options.bake_html.item_count {
            break;
        }
    }
    let html_part = ul.to_html_string();

    let content_vec = fs::read(&config.rss_options.bake_html.target_html).unwrap();
    let content = String::from_utf8_lossy(&content_vec);
    let re = Regex::new(r"<!-- rss insertion begin -->(\n|.)*<!-- rss insertion end -->").unwrap();
    String::from(re.replace(
        &content,
        "<!-- rss insertion begin -->\n".to_owned() + &html_part + "\n<!-- rss insertion end -->",
    ))
}

/// RSS作る
pub fn make_rss<W: std::io::Write>(
    env: &String,
    config: &UploaderConfig,
    repo: &Repository,
    ignore: &regex::RegexSet,
    writer: W,
) -> (Result<W, rss::Error>, String) {
    let baseurl = &config.environments[env].url;

    let mut walk = repo.revwalk().unwrap();
    walk.push_head().unwrap();

    let mut i = 10;
    let mut items = Vec::with_capacity(10);
    for oid in walk.by_ref().filter_map(Result::ok) {
        if let Ok(commit) = repo.find_commit(oid) {
            let rss_item = make_rss_item_from_commit(repo, &commit, ignore, baseurl);
            if rss_item.is_some() {
                items.push(rss_item.unwrap());
                i -= 1;
            }
        }
        if i <= 0 {
            break;
        }
    }

    // bake HTML
    let baked_html = bake_rss_to_html(config, &items);

    // 画像
    let image = rss::ImageBuilder::default()
        .url(make_url_for_path(baseurl, &config.rss_options.image_file))
        .title(&config.rss_options.title)
        .link(baseurl)
        .build();

    let now = chrono::Utc::now().to_rfc2822();
    let mut channel = ChannelBuilder::default()
        .title(&config.rss_options.title)
        .link(baseurl)
        .description(&config.rss_options.description)
        .language(Some(String::from(&config.rss_options.language)))
        .pub_date(Some(String::from(&now)))
        .last_build_date(Some(now))
        .items(items)
        .image(Some(image))
        .build();
    if let Some(opensearch) = &config.rss_options.open_search {
        channel.set_atom_ext(rss::extension::atom::AtomExtension {
            links: vec![rss::extension::atom::Link {
                rel: "search".into(),
                href: String::from(&opensearch.href),
                mime_type: Some("application/opensearchdescription+xml".into()),
                title: opensearch.title.clone(),
                ..Default::default()
            }],
        });
    }

    (channel.write_to(writer), baked_html)
}
