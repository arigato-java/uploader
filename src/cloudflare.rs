use crate::*;
use anyhow::Result;
use reqwest::header::AUTHORIZATION;
use reqwest_middleware::ClientWithMiddleware;
use serde::Serialize;
use std::sync::Arc;

#[derive(Serialize)]
struct CloudflarePurgeByUrl {
    files: Vec<String>,
}

// 指定されたパスに対応するURLをCloudflareでPurgeする
pub async fn cloudflare_purge<'a, T: Iterator<Item = &'a Arc<Path>>>(
    client: &ClientWithMiddleware,
    zone_id: &String,
    site_url: &str,
    mut paths: T,
) -> Result<()> {
    let mut path_left = true;
    let cloudflare_key = std::env::var("CLOUDFLARE_KEY");
    let url = format!(
        "https://api.cloudflare.com/client/v4/zones/{}/purge_cache",
        zone_id
    );
    while path_left {
        let mut url_list = Vec::new();
        for path in &mut paths {
            let path_str = path.to_string_lossy();
            url_list.push(make_url_for_path(&site_url, &path_str));
            if url_list.len() >= 30 {
                break;
            }
        }
        let url_list_len = url_list.len();
        if url_list_len > 0 {
            let request_obj = CloudflarePurgeByUrl { files: url_list };
            if let Ok(key) = &cloudflare_key {
                let response = client
                    .post(&url)
                    .header(AUTHORIZATION.as_str(), format!("Bearer {}", key))
                    .json(&request_obj)
                    .send()
                    .await?
                    .text()
                    .await?;
                println!("Cloudflare purge response: {:?}", response);
            } else {
                println!(
                    "CLOUDFLARE_KEY not set. Would have posted: {}",
                    serde_json::to_string(&request_obj)?
                );
            }
        }
        if url_list_len < 30 {
            path_left = false;
        }
    }

    Ok(())
}
