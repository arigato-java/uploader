use crate::*;
use anyhow::Result;
use chrono::{DateTime, FixedOffset};
use sitemap::structs::UrlEntry;
use sitemap::writer::SiteMapWriter;
use std::collections::{BTreeMap, HashSet};
use std::ffi::{OsStr, OsString};
use std::rc::Rc;
use walkdir::WalkDir;

fn find_last_update(
    repo: &Repository,
    mut path_set: HashSet<Rc<OsStr>>,
) -> BTreeMap<Rc<OsStr>, DateTime<FixedOffset>> {
    let mut res = BTreeMap::new();

    let mut walk = repo.revwalk().unwrap();
    walk.push_head().unwrap();

    for oid in walk.by_ref().filter_map(Result::ok) {
        if let Ok(commit) = repo.find_commit(oid) {
            let tree_new = &commit.tree().ok();
            let parent = commit.parent(0);
            if parent.is_err() {
                continue;
            }
            let tree_old = &parent.unwrap().tree().ok();
            let diff = repo
                .diff_tree_to_tree(tree_old.as_ref(), tree_new.as_ref(), None)
                .unwrap();

            diff.foreach(
                &mut |delta: git2::DiffDelta, _val: f32| match delta.new_file().path() {
                    Some(path) => {
                        let path_ref = path_set.get(path.as_os_str());
                        if path_ref.is_none() {
                            return true;
                        }
                        let path = path_ref.unwrap().clone();

                        let commit_datetime_local = get_commit_localtime(&commit);

                        res.insert(path.clone(), commit_datetime_local);
                        path_set.remove(&path);
                        true
                    }
                    _ => true,
                },
                None,
                None,
                None,
            )
            .unwrap();
        }
        if path_set.is_empty() {
            break;
        }
    }
    res
}

pub fn make_sitemap<W: std::io::Write + core::marker::Sized>(
    env: &String,
    config: &UploaderConfig,
    repo: &Repository,
    ignore: &regex::RegexSet,
    writer: W,
) -> Result<()> {
    let baseurl = &config.environments[env].url;

    let mut path_set = HashSet::<Rc<OsStr>>::with_capacity(500);
    let mut path_cache = HashMap::<Rc<OsStr>, String>::with_capacity(1000);
    for entry in WalkDir::new(".")
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| {
            !e.file_type().is_dir()
                && if let Some(ext) = e.path().extension() {
                    ext == "html" || ext == "htm" || ext == "txt"
                } else {
                    false
                }
        })
    {
        let relative_path = &entry.path().strip_prefix("./").unwrap();
        let relative_path_str = relative_path.to_str().unwrap();
        if ignore.is_match(relative_path_str) {
            continue;
        }
        let key: Rc<OsStr> = OsString::from(relative_path).into();
        path_set.insert(key.clone());
        path_cache.insert(key, relative_path_str.into());
    }

    let last_update_time = find_last_update(repo, path_set);

    let sitemap_writer = SiteMapWriter::new(writer);
    let mut urlwriter = sitemap_writer.start_urlset()?;
    for (path, lastmod) in last_update_time {
        let anurl = UrlEntry::builder()
            .loc(make_url_for_path(
                baseurl,
                &path_cache.get(&path).unwrap().as_ref(),
            ))
            .lastmod(lastmod)
            .build()?;
        urlwriter.url(anurl)?;
    }
    urlwriter.end()?;
    Ok(())
}
