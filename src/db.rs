use anyhow::anyhow;
use aws_config::SdkConfig;
use aws_sdk_dynamodb::{
    config::Region, operation::put_item::PutItemOutput, types::AttributeValue, Client,
};

const BRANCH_KEY: &str = "Branch";
const COMMITID_KEY: &str = "CommitId";

pub async fn make_client(base_cfg: &SdkConfig, region: &str) -> Client {
    let cfg = base_cfg
        .clone()
        .into_builder()
        .region(Region::new(String::from(region)))
        .build();
    Client::new(&cfg)
}

pub async fn put_commitid(
    client: &Client,
    table_name: &str,
    key: &str,
    commitid: &str,
) -> anyhow::Result<PutItemOutput> {
    let res = client
        .put_item()
        .table_name(table_name)
        .item(BRANCH_KEY, AttributeValue::S(key.into()))
        .item(COMMITID_KEY, AttributeValue::S(commitid.into()))
        .send()
        .await?;
    Ok(res)
}

pub async fn get_commitid(client: &Client, table_name: &str, key: &str) -> anyhow::Result<String> {
    let query_res = client
        .get_item()
        .table_name(table_name)
        .key(BRANCH_KEY, AttributeValue::S(key.into()))
        .send()
        .await?;
    let item = query_res.item().unwrap().get(COMMITID_KEY).unwrap();

    if let AttributeValue::S(x) = item {
        Ok(String::from(x))
    } else {
        Err(anyhow!("DB value type mismatch"))
    }
}
