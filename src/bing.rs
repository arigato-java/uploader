use crate::*;
use anyhow::Result;
use reqwest_middleware::ClientWithMiddleware;
use serde::Serialize;
use std::path::Path;
use std::sync::Arc;

#[derive(Serialize)]
#[allow(non_snake_case)]
struct BingSubmitUrlBatchObj {
    siteUrl: String,
    urlList: Vec<String>,
}

pub async fn bing_submit_url_batch<'a, T: Iterator<Item = &'a Arc<Path>>>(
    client: &ClientWithMiddleware,
    site_url: &str,
    paths: T,
) -> Result<()> {
    let site_url_without_trailing_slash = site_url[0..site_url.len() - 1].to_string();
    let mut url_list = Vec::new();
    for path in paths {
        let path_str = path.to_string_lossy();
        url_list.push(make_url_for_path(&site_url, &path_str));
    }
    let num_urls = url_list.len();
    if num_urls == 0 {
        return Ok(());
    }
    let req_obj = BingSubmitUrlBatchObj {
        siteUrl: site_url_without_trailing_slash,
        urlList: url_list,
    };
    if let Ok(url) = nonempty_envvar("BING_SUBMIT_URL") {
        client.post(url).json(&req_obj).send().await?;
        println!("Submitted {} updated URLs to Bing", num_urls);
    } else {
        println!(
            "BING_SUBMIT_URL not set. Would have posted: {}",
            serde_json::to_string(&req_obj)?
        );
    }

    Ok(())
}
