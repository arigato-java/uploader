use crate::make_url_for_path;
use anyhow::Result;
use chrono::Utc;
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;

/// 指定されたパスに対応するURLをCloudFrontで無効にする
pub async fn invalidate_files<'a, T: Iterator<Item = &'a Arc<Path>>>(
    aws_cfg: &aws_types::SdkConfig,
    distribution_id: &String,
    paths: T,
) -> Result<()> {
    let mut urls = Vec::new();
    for path in paths {
        let path_str = path.to_string_lossy();
        let url = make_url_for_path(&"/", &path_str);
        urls.push(url)
    }
    let urls_count = urls.len();
    if urls_count == 0 {
        return Ok(());
    }
    let retry_config = aws_sdk_cloudfront::config::retry::RetryConfig::standard()
        .with_initial_backoff(Duration::from_secs(1))
        .with_max_attempts(3);
    let config = aws_sdk_cloudfront::config::Builder::from(aws_cfg)
        .retry_config(retry_config)
        .build();
    let client = aws_sdk_cloudfront::Client::from_conf(config);
    let cfn_paths = aws_sdk_cloudfront::types::Paths::builder()
        .set_items(Some(urls))
        .set_quantity(Some(urls_count.try_into().unwrap()))
        .build()?;
    let dt = Utc::now().timestamp_nanos_opt().unwrap().to_string();
    let batch = aws_sdk_cloudfront::types::InvalidationBatch::builder()
        .paths(cfn_paths)
        .caller_reference(dt)
        .build()?;
    let res = client
        .create_invalidation()
        .distribution_id(distribution_id)
        .invalidation_batch(batch)
        .send();

    res.await?;
    Ok(())
}
