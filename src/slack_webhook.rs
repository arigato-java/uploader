use reqwest_middleware::ClientWithMiddleware;
use serde::Serialize;
use std::path::Path;
use std::sync::Arc;
use truncrate::*;

use crate::nonempty_envvar;

#[derive(Serialize)]
struct SlackWebhookPostObj {
    text: String,
}

pub async fn notify(
    client: &ClientWithMiddleware,
    res: &anyhow::Result<Vec<Arc<Path>>>,
) -> anyhow::Result<()> {
    let body_long = match res {
        Ok(files) => files.iter().fold(
            String::with_capacity(4096) + "デプロイ成功\n",
            |acc, p| acc + p.to_str().unwrap_or("") + "\n",
        ),
        Err(e) => e.to_string(),
    };
    let req_obj = SlackWebhookPostObj {
        text: body_long.truncate_to_boundary(400).to_string(),
    };
    if let Ok(url) = nonempty_envvar("SLACK_WEBHOOK_URL") {
        client.post(url).json(&req_obj).send().await?;
    } else {
        println!(
            "SLACK_WEBHOOK_URL not set. Would have posted: {}",
            serde_json::to_string(&req_obj)?
        );
    }

    Ok(())
}
