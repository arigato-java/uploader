use crate::{uploader_aws_config, UploaderConfig};
use anyhow::Result;
use aws_sdk_s3::primitives::ByteStream;
use bytes::{Buf, BufMut, Bytes};
use futures::future::{BoxFuture, TryFutureExt};
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Semaphore;
use tokio::task;

pub struct UploadEntry {
    pub(crate) path: Arc<Path>,
    pub(crate) content_type: Option<&'static str>,
    pub(crate) cache_control: Option<&'static str>,
    // NoneならCompress可の扱い。ムービーを圧縮すると再生できない環境があるため。
    pub(crate) compress: Option<bool>,
}

impl UploadEntry {
    pub fn new(path: Arc<Path>) -> UploadEntry {
        UploadEntry {
            path,
            content_type: None,
            cache_control: None,
            compress: None,
        }
    }
}

pub struct UploadDestinationS3 {
    client: aws_sdk_s3::Client,
    bucket: &'static str,
}

impl UploadDestinationS3 {
    pub fn new(client: aws_sdk_s3::Client, bucket: &'static str) -> UploadDestinationS3 {
        UploadDestinationS3 { client, bucket }
    }
    fn put_object(
        &self,
        key: impl Into<String>,
        content_type: impl Into<String>,
        cache_control: impl Into<String>,
        content_encoding: Option<&str>,
        body: bytes::Bytes,
    ) -> BoxFuture<'static, Result<()>> {
        // S3では、 /path/to/index.html にアップロードするとき /path/to/ というキーでもアップロードしておく
        let key_string: String = key.into();
        let content_type_string: String = content_type.into();
        let cache_control_string: String = cache_control.into();
        let dir_path = key_string.strip_suffix("/index.html");
        let d = if let Some(dp) = dir_path {
            let client_with_options = self
                .client
                .put_object()
                .bucket(self.bucket)
                .key(dp.to_owned() + "/")
                .content_type(&content_type_string)
                .cache_control(&cache_control_string)
                .body(ByteStream::from(body.clone()));
            let client_with_encoding = if let Some(ce) = content_encoding {
                client_with_options.content_encoding(ce)
            } else {
                client_with_options
            };
            Some(client_with_encoding.send())
        } else {
            None
        };

        let client_without_encoding = self
            .client
            .put_object()
            .bucket(self.bucket)
            .key(&key_string)
            .content_type(&content_type_string)
            .cache_control(&cache_control_string)
            .body(ByteStream::from(body));
        let client_with_encoding2 = if let Some(ce) = content_encoding {
            client_without_encoding.content_encoding(ce)
        } else {
            client_without_encoding
        };
        let f = client_with_encoding2.send();

        if let Some(dd) = d {
            Box::pin(
                f.and_then(|_| dd)
                    .err_into::<anyhow::Error>()
                    .and_then(|_| futures::future::ok(())),
            )
        } else {
            Box::pin(
                f.err_into::<anyhow::Error>()
                    .and_then(|_| futures::future::ok(())),
            )
        }
    }
    fn delete_object(&self, key: impl Into<String>) -> BoxFuture<'static, Result<()>> {
        let key_string: String = key.into();
        let dir_path = key_string.strip_suffix("/index.html");
        let d = dir_path.map(|dp| {
            self.client
                .delete_object()
                .bucket(self.bucket)
                .key(dp.to_owned() + "/")
                .send()
        });
        let f = self
            .client
            .delete_object()
            .bucket(self.bucket)
            .key(&key_string)
            .send();
        if let Some(dd) = d {
            Box::pin(
                f.and_then(|_| dd)
                    .err_into::<anyhow::Error>()
                    .and_then(|_| futures::future::ok(())),
            )
        } else {
            Box::pin(
                f.err_into::<anyhow::Error>()
                    .and_then(|_| futures::future::ok(())),
            )
        }
    }
}

/// S3処理用の宛先が入ったやつを作る
pub async fn prepare_destinations(
    env: &str,
    config: &'static UploaderConfig,
) -> Arc<[UploadDestinationS3]> {
    let mut destinations_draft = Vec::new();
    let timeout_config = aws_config::timeout::TimeoutConfig::builder()
        .operation_timeout(Duration::from_secs(1200))
        .connect_timeout(Duration::from_secs(60))
        .build();
    let retry_config = aws_sdk_s3::config::retry::RetryConfig::standard()
        .with_initial_backoff(Duration::from_secs(1))
        .with_max_attempts(3);

    let env = &config.environments[env];
    let provider = if env.bucket_aws_profile.is_some() {
        use aws_config::profile::ProfileFileCredentialsProvider;
        use aws_credential_types::provider::SharedCredentialsProvider;
        Some(SharedCredentialsProvider::new(
            ProfileFileCredentialsProvider::builder()
                .profile_name(env.bucket_aws_profile.as_ref().unwrap())
                .build(),
        ))
    } else {
        None
    };
    for bucket_config in &env.buckets {
        let cfg_base = uploader_aws_config().await;
        let mut s3_cfg_build = aws_sdk_s3::config::Builder::from(cfg_base)
            .timeout_config(timeout_config.clone())
            .retry_config(retry_config.clone())
            .region(aws_config::Region::new(&bucket_config.region));
        if provider.is_some() {
            s3_cfg_build = s3_cfg_build.credentials_provider(provider.clone().unwrap());
        }
        let s3_cfg = match &bucket_config.endpoint {
            Some(endpoint_url) => s3_cfg_build.endpoint_url(endpoint_url),
            None => s3_cfg_build,
        }
        .build();
        destinations_draft.push(UploadDestinationS3::new(
            aws_sdk_s3::Client::from_conf(s3_cfg),
            &bucket_config.name,
        ))
    }
    destinations_draft.into()
}

/// 単一のオブジェクトをアップロード
pub async fn upload_single_entry(
    destinations: &Arc<[UploadDestinationS3]>,
    metadata: UploadEntry,
    content: bytes::Bytes,
) -> Result<()> {
    // 圧縮してみて、実際小さくなれば圧縮したほうを使いたい
    let try_compress = metadata.compress.is_none() || metadata.compress == Some(true);
    let mut content_encoding = None;
    let compressed_bytes = if try_compress {
        let content_cloned = content.clone();
        let compressed = task::spawn_blocking(move || {
            let mut compressed = bytes::BytesMut::with_capacity(if try_compress {
                2 * content_cloned.len()
            } else {
                0
            });
            zopfli::compress(
                zopfli::Options::default(),
                zopfli::Format::Gzip,
                content_cloned.reader(),
                (&mut compressed).writer(),
            )
            .ok();
            compressed
        })
        .await?;

        let original_size = content.len();
        let compressed_size = compressed.len();
        println!(
            "Compress {}\t{} -> {}",
            metadata.path.display(),
            original_size,
            compressed_size
        );
        if original_size > compressed_size {
            content_encoding = Some("gzip")
        }
        Some(compressed.freeze())
    } else {
        None
    };

    let dest_obj = (*metadata.path).to_string_lossy().to_string();
    for destination in destinations.iter() {
        let body = if content_encoding.is_none() {
            content.clone()
        } else {
            compressed_bytes.clone().unwrap()
        }; // shallow cloning
        destination
            .put_object(
                &dest_obj,
                metadata.content_type.unwrap(),
                metadata.cache_control.unwrap(),
                content_encoding,
                body,
            )
            .await?;
    }
    Ok(())
}

/// パスからメタデータ決めてアップロード叩く
pub async fn upload_files<'a, T: Iterator<Item = &'a Arc<Path>>>(
    destinations: &Arc<[UploadDestinationS3]>,
    config: &'static UploaderConfig,
    paths: T,
) -> Result<()> {
    let regexset =
        Arc::new(regex::RegexSet::new(config.metadata_rules.iter().map(|mr| &mr.rule)).unwrap());
    let sem = Arc::new(Semaphore::new(4));
    let mut tasks = Vec::new();

    for path in paths {
        let permit = Arc::clone(&sem).acquire_owned().await;
        let path_cloned = Arc::clone(path);
        let destinations_cloned = Arc::clone(destinations);
        let regexset_cloned = Arc::clone(&regexset);
        let join_handle = task::spawn(async move {
            let _permit = permit;

            let metadata = regexset_cloned
                .matches(path_cloned.to_str().unwrap())
                .iter()
                .fold(UploadEntry::new(path_cloned), |mut acc, x| {
                    let mr = &config.metadata_rules[x];
                    acc.content_type = acc.content_type.or(mr.content_type.as_deref());
                    acc.cache_control = acc.cache_control.or(mr.cache_control.as_deref());
                    acc.compress = acc.compress.or(mr.compress);
                    acc
                });
            println!(
                "Upload {}\t{:?}\t{:?}\t{:?}",
                metadata.path.display(),
                metadata.content_type,
                metadata.cache_control,
                metadata.compress.unwrap_or(true)
            );

            let data = tokio::fs::read(&metadata.path).await.unwrap();
            let bytes = Bytes::from(data);
            upload_single_entry(&destinations_cloned, metadata, bytes)
                .await
                .unwrap();
        });
        tasks.push(join_handle);
    }
    for task in tasks {
        task.await?;
    }
    Ok(())
}

/// 指定されたパスを削除する
pub async fn remove_files<'a, T: Iterator<Item = &'a Arc<Path>>>(
    destinations: &Arc<[UploadDestinationS3]>,
    paths: T,
) -> Result<()> {
    let sem = Arc::new(Semaphore::new(4));
    let mut tasks = Vec::new();

    for path in paths {
        for (destination_idx, _) in destinations.iter().enumerate() {
            let permit = Arc::clone(&sem).acquire_owned().await;
            let path_cloned = Arc::clone(path);
            let destinations_owned = Arc::clone(destinations);
            let join_handle = task::spawn(async move {
                let _permit = permit;
                destinations_owned[destination_idx]
                    .delete_object(path_cloned.to_string_lossy())
                    .await
                    .unwrap();
            });
            tasks.push(join_handle);
        }
    }
    for join_handle in tasks {
        join_handle.await?;
    }
    Ok(())
}
