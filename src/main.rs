use anyhow::Context;
use aws_smithy_experimental::hyper_1_0::{CryptoMode, HyperClientBuilder};
use bytes::Bytes;
use chrono::{DateTime, FixedOffset, TimeZone};
use clap::{Parser, Subcommand};
use git2::Repository;
use reqwest_middleware::{ClientBuilder, ClientWithMiddleware};
use reqwest_retry::{policies::ExponentialBackoff, RetryTransientMiddleware};
use serde::Deserialize;
use std::boxed::Box;
use std::collections::HashMap;
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tokio::sync::OnceCell;

mod upload;
use upload::*;
mod rss_writer;
use rss_writer::*;
mod bing;
mod sitemap_writer;
use bing::*;
mod cloudflare;
mod cloudfront;
mod db;
mod slack_webhook;

const USER_AGENT_STR: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

pub fn nonempty_envvar<K: AsRef<std::ffi::OsStr>>(key: K) -> Result<String, std::env::VarError> {
    let res = std::env::var(key);
    match res {
        Ok(val) => {
            if !val.is_empty() {
                Ok(val)
            } else {
                Err(std::env::VarError::NotPresent)
            }
        }
        _ => res,
    }
}

fn make_url_for_path<T: Deref<Target = str>, U: Deref<Target = str>>(
    baseurl: &T,
    path: &U,
) -> std::string::String {
    let baseurl_len = baseurl.len();
    let path_len = path.len();
    let mut res = String::with_capacity(baseurl_len + path_len);
    res.push_str(baseurl);
    if path.ends_with("index.html") {
        res.push_str(&path[0..(path_len - 10)])
    } else {
        res.push_str(path);
    }
    res
}

fn get_commit_localtime(commit: &git2::Commit) -> DateTime<FixedOffset> {
    let commit_when = commit.committer().when();
    let commit_offset = FixedOffset::east_opt(commit_when.offset_minutes() * 60).unwrap();
    let commit_datetime_utc = DateTime::from_timestamp(commit_when.seconds(), 0)
        .unwrap()
        .naive_utc();
    commit_offset.from_utc_datetime(&commit_datetime_utc)
}

type AddedModified = (Vec<Arc<Path>>, Vec<Arc<Path>>);
fn git_diff(
    repo: &Repository,
    from: &str,
    to: &str,
    ignore: &regex::RegexSet,
) -> Result<AddedModified, git2::Error> {
    let git_last_upload = repo.revparse_single(from)?.peel_to_tree().ok();
    let git_head = repo.revparse_single(to)?.peel_to_tree().ok();
    let diff = repo.diff_tree_to_tree(git_last_upload.as_ref(), git_head.as_ref(), None)?;

    let mut added = Vec::<Arc<Path>>::with_capacity(30);
    let mut modified = Vec::<Arc<Path>>::with_capacity(30);

    diff.foreach(
        &mut |delta: git2::DiffDelta, _val: f32| {
            if let Some(path) = delta.new_file().path() {
                if ignore.is_match(path.to_str().unwrap()) {
                    return true;
                }
                let pb = || PathBuf::from(path).into();
                match delta.status() {
                    git2::Delta::Added => {
                        added.push(pb());
                    }
                    git2::Delta::Modified => {
                        modified.push(pb());
                    }
                    _ => (),
                }
            }
            true
        },
        None,
        None,
        None,
    )?;

    Ok((added, modified))
}

/// Static Website Uploader
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to yaml configuration file
    #[clap(short, long)]
    config: PathBuf,

    /// Envronment (prod, staging)
    #[clap(short, long, default_value_t = String::from("staging"))]
    env: String,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Upload chaged files and notify external services
    Upload {
        /// Last uploaded commit revision
        #[clap(required = false)]
        uploaded: Option<String>,
    },
    /// Put specified files to storage
    Copy {
        #[clap(required = true, value_parser = wrap_arcpath)]
        files: Vec<Arc<Path>>,
    },
    /// Delete specified objects from storage
    Delete {
        #[clap(required = true, value_parser = wrap_arcpath)]
        files: Vec<Arc<Path>>,
    },
    /// Generate RSS
    Rss {
        #[clap(required = true)]
        output: PathBuf,
    },
    /// Generate Sitemap
    Sitemap {
        #[clap(required = true)]
        output: PathBuf,
    },
    /// Invalidate CDN
    Invalidate {
        #[clap(required = true, value_parser = wrap_arcpath)]
        files: Vec<Arc<Path>>,
    },
    /// Submit URL to search engine
    SubmitURL {
        #[clap(required = true, value_parser = wrap_arcpath)]
        files: Vec<Arc<Path>>,
    },
    /// Test slack notification
    Slack {
        #[clap(required = true, value_parser = wrap_arcpath)]
        files: Vec<Arc<Path>>,
    },
    /// Update uploaded commit id to DynamoDB
    SetUploaded {
        /// commit hash
        #[clap(required = true)]
        uploaded: String,
    },
    /// Get uploaded commit id from DynamoDB
    GetUploaded,
}

fn wrap_arcpath(s: &str) -> Result<Arc<Path>, String> {
    Ok(PathBuf::from(s).into())
}

// Yaml の設定ファイル
#[derive(Deserialize)]
pub struct UploaderConfig {
    /// オブジェクトに設定するメタデータのルール
    metadata_rules: Vec<MetadataRule>,
    /// デプロイ先環境
    environments: HashMap<String, Environment>,
    /// 無視するファイルのregex
    ignore_path: Vec<String>,
    /// RSSのオプション
    rss_options: RssWriterOptions,
    /// sitemapのオプション
    sitemap: SitemapOptions,
    /// DynamoDB接続オプション
    dynamodb: Option<DynamoDbOptions>,
}

#[derive(Deserialize)]
struct MetadataRule {
    /// 正則表現
    rule: String,
    /// Cache-Control
    cache_control: Option<String>,
    /// Content-Type
    content_type: Option<String>,
    /// 圧縮可か
    compress: Option<bool>,
}

#[derive(Deserialize)]
struct Environment {
    /// URL
    url: String,
    /// バケット一覧
    buckets: Vec<BucketConfig>,
    /// AWS Profile名。ストレージとしてAWS外のS3互換サービスを使うときに利用するとDynamoDBと使い分けられて便利。
    bucket_aws_profile: Option<String>,
    /// CloudFront Distribution ID
    distribution_id: Option<String>,
    /// Cloudflare Zone ID
    cloudflare_zone: Option<String>,
    /// DynamoDBでアップロードしたコミットIDをとっておくためのキー名
    dynamodb_key: Option<String>,
}

#[derive(Deserialize)]
struct BucketConfig {
    /// バケット名
    name: String,
    /// リージョン
    region: String,
    /// API Endpoint
    endpoint: Option<String>,
}

#[derive(Deserialize)]
struct SitemapOptions {
    /// sitemapに入れないファイルのregex (ignore_pathといずれかにマッチすると無視)
    ignore_path: Vec<String>,
    /// sitemapアップロード時のcontent-type
    content_type: String,
    /// sitemapアップロード時のcache-control
    cache_control: String,
    /// Sitemapのファイル名
    path: Arc<Path>,
}

#[derive(Deserialize)]
struct DynamoDbOptions {
    /// DynamoDBのテーブル名
    table: String,
    /// リージョン
    region: String,
}
fn reqwest_client_with_backoff() -> ClientWithMiddleware {
    let retry_policy = ExponentialBackoff::builder().build_with_max_retries(2);
    ClientBuilder::new(
        reqwest::Client::builder()
            .user_agent(USER_AGENT_STR)
            .build()
            .unwrap(),
    )
    .with(RetryTransientMiddleware::new_with_policy(retry_policy))
    .build()
}

static UPLOADER_AWS_CONFIG: OnceCell<aws_config::SdkConfig> = OnceCell::const_new();

async fn init_uploader_aws_config() -> aws_config::SdkConfig {
    let http_client = HyperClientBuilder::new()
        .crypto_mode(CryptoMode::AwsLc)
        .build_https();

    aws_config::defaults(aws_config::BehaviorVersion::latest())
        .app_name(
            aws_config::AppName::new(concat!(
                env!("CARGO_PKG_NAME"),
                "-",
                env!("CARGO_PKG_VERSION")
            ))
            .expect("invalid app name for aws sdk config"),
        )
        .http_client(http_client)
        .load()
        .await
}

pub async fn uploader_aws_config() -> &'static aws_config::SdkConfig {
    UPLOADER_AWS_CONFIG
        .get_or_init(init_uploader_aws_config)
        .await
}

async fn upload_flush_and_mark(
    args: &Args,
    uploaded: Option<&str>,
    uploader_config: &'static UploaderConfig,
    reqwest_client: &ClientWithMiddleware,
) -> anyhow::Result<Vec<Arc<Path>>> {
    // get last uploaded commit id
    let aws_cfg = uploader_aws_config().await;
    let dynamodb_cfg_opt = &uploader_config.dynamodb.as_ref();
    let db_client = if let Some(dynamodb_cfg) = dynamodb_cfg_opt {
        Some(db::make_client(aws_cfg, &dynamodb_cfg.region).await)
    } else {
        None
    };
    let upload_env = &uploader_config
        .environments
        .get(&args.env)
        .context("specified environment not found in config")?;
    let uploaded_ref = if let Some(r) = uploaded {
        String::from(r)
    } else {
        let key = upload_env.dynamodb_key.as_ref().context(
            "DynamoDB key is not specified for the environment and no uploaded ref is specified.",
        )?;
        db::get_commitid(db_client.as_ref().context("DynamoDB client creation failed")?, &dynamodb_cfg_opt.context("DynamoDB configuration not found in config and no uploaded ref id is specified.")?.table, key)
            .await?
    };

    let ignore_set = regex::RegexSet::new(uploader_config.ignore_path.iter())?;
    let sitemap_ignore_set = regex::RegexSet::new(
        uploader_config
            .ignore_path
            .iter()
            .chain(uploader_config.sitemap.ignore_path.iter()),
    )?;
    let repo = Repository::open(".")?;
    let (added, modified) = git_diff(&repo, &uploaded_ref, "HEAD", &ignore_set)?;

    let files_to_upload = added.iter().chain(modified.iter()).filter(|p| {
        // rss焼き込み対象のファイルは焼き込み後にアップロードするのでここでは扱わない。
        uploader_config.rss_options.bake_html.target_html != **p
    });
    let destinations = prepare_destinations(&args.env, uploader_config).await;

    let upload_all_job = upload_files(&destinations, uploader_config, files_to_upload);

    let mut rss_bake_html_is_changed = false;
    let html_changed: Vec<&Arc<Path>> = added
        .iter()
        .chain(modified.iter())
        .filter(|p| {
            // rss焼き込んだhtml が変更されている際にinvalidationが重複するのを防ぐフラグ
            rss_bake_html_is_changed |= uploader_config.rss_options.bake_html.target_html == **p;
            let ext = p.extension();
            ext.is_some_and(|suffix| suffix == "html")
        })
        .filter(|p| {
            // sitemap で無視するものはrss にもださない。
            let s = p.to_str();
            s.is_some_and(|ss| !sitemap_ignore_set.is_match(ss))
        })
        .collect();

    let sitemap_path = uploader_config.sitemap.path.clone();
    let rss_path = uploader_config.rss_options.path.clone();
    if !html_changed.is_empty() {
        let mut rss_buf = Vec::with_capacity(64 * 1024);
        let (res, baked_html) =
            rss_writer::make_rss(&args.env, uploader_config, &repo, &ignore_set, &mut rss_buf);
        res?;
        let rss_data = Bytes::from(rss_buf);
        let baked_html_bytes = Bytes::from(baked_html);

        let mut sitemap_buf = Vec::with_capacity(64 * 1024);
        sitemap_writer::make_sitemap(
            &args.env,
            uploader_config,
            &repo,
            &sitemap_ignore_set,
            &mut sitemap_buf,
        )?;
        let sitemap_data = Bytes::from(sitemap_buf);

        let upload_rss_job = upload_single_entry(
            &destinations,
            UploadEntry {
                path: rss_path.clone(),
                content_type: Some(&uploader_config.rss_options.upload_options.content_type),
                cache_control: Some(&uploader_config.rss_options.upload_options.cache_control),
                compress: None,
            },
            rss_data,
        );
        let upload_baked_rss_job = upload_single_entry(
            &destinations,
            UploadEntry {
                path: Arc::clone(&uploader_config.rss_options.bake_html.target_html),
                content_type: Some(
                    &uploader_config
                        .rss_options
                        .bake_html
                        .html_options
                        .content_type,
                ),
                cache_control: Some(
                    &uploader_config
                        .rss_options
                        .bake_html
                        .html_options
                        .cache_control,
                ),
                compress: None,
            },
            baked_html_bytes,
        );
        let upload_sitemap_job = upload_single_entry(
            &destinations,
            UploadEntry {
                path: sitemap_path.clone(),
                content_type: Some(&uploader_config.sitemap.content_type),
                cache_control: Some(&uploader_config.sitemap.cache_control),
                compress: None,
            },
            sitemap_data,
        );
        upload_rss_job.await?;
        upload_baked_rss_job.await?;
        upload_sitemap_job.await?;
    }
    upload_all_job.await?;

    // Invalidate cache
    let rss_and_sitemap = if html_changed.is_empty() {
        vec![]
    } else if rss_bake_html_is_changed {
        vec![rss_path, sitemap_path]
    } else {
        vec![
            rss_path,
            sitemap_path,
            Arc::clone(&uploader_config.rss_options.bake_html.target_html),
        ]
    };
    let files_to_invalidate = modified.iter().chain(rss_and_sitemap.iter());
    if let Some(distribution_id) = &upload_env.distribution_id {
        cloudfront::invalidate_files(aws_cfg, distribution_id, files_to_invalidate.clone())
            .await?;
    }
    if let Some(zone_id) = &upload_env.cloudflare_zone {
        let site_url = &uploader_config.environments[&args.env].url;
        cloudflare::cloudflare_purge(reqwest_client, zone_id, site_url, files_to_invalidate)
            .await?;
    }

    // uploadedの箇所をdynamodb に書いておく
    if uploaded.is_none() {
        let head_id = repo
            .head()?
            .target()
            .context("Oid of head is not returned.")?
            .to_string();
        let key = upload_env
            .dynamodb_key
            .as_ref()
            .context("DynamoDB key for environment is not specified.")?;
        db::put_commitid(
            &db_client.context("DynamoDB client is None.")?,
            &dynamodb_cfg_opt
                .context("DynamoDB configuration is not found in the config file")?
                .table,
            key,
            &head_id,
        )
        .await?;
    }

    bing_submit_url_batch(reqwest_client, &upload_env.url, html_changed.into_iter()).await?;
    Ok(modified.into_iter().chain(added.into_iter()).collect())
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    let f = std::fs::File::open(&args.config).expect("Could not open config file.");
    let uploader_config_box: Box<UploaderConfig> =
        Box::new(serde_yaml_ng::from_reader(f).expect("Could not read config."));
    let uploader_config: &'static UploaderConfig = Box::leak(uploader_config_box);

    match &args.command {
        Commands::Upload { uploaded } => {
            println!("{}", USER_AGENT_STR);
            let reqwest_client = reqwest_client_with_backoff();
            let upload_result =
                upload_flush_and_mark(&args, uploaded.as_deref(), uploader_config, &reqwest_client)
                    .await;
            slack_webhook::notify(&reqwest_client, &upload_result)
                .await
                .unwrap();
            upload_result.expect("upload");
        }
        Commands::Copy { files } => {
            let destinations = prepare_destinations(&args.env, uploader_config).await;
            upload_files(&destinations, uploader_config, files.iter())
                .await
                .unwrap();
        }
        Commands::Delete { files } => {
            let destinations = prepare_destinations(&args.env, uploader_config);
            remove_files(&destinations.await, files.iter())
                .await
                .unwrap();
        }
        Commands::Rss { output } => {
            let out = std::fs::File::create(output).unwrap();

            let ignore_set = regex::RegexSet::new(uploader_config.ignore_path.iter()).unwrap();
            let repo = Repository::open(".").ok().unwrap();

            let (res, bake_html) =
                rss_writer::make_rss(&args.env, uploader_config, &repo, &ignore_set, out);
            println!("{}", bake_html);
            res.unwrap();
        }
        Commands::Sitemap { output } => {
            let out = std::fs::File::create(output).unwrap();

            let ignore_set = regex::RegexSet::new(
                uploader_config
                    .ignore_path
                    .iter()
                    .chain(uploader_config.sitemap.ignore_path.iter()),
            )
            .unwrap();
            let repo = Repository::open(".").ok().unwrap();

            sitemap_writer::make_sitemap(&args.env, uploader_config, &repo, &ignore_set, out)
                .unwrap();
        }
        Commands::Invalidate { files } => {
            // Cloudfront
            if let Some(distribution_id) = &uploader_config.environments[&args.env].distribution_id
            {
                let aws_cfg = uploader_aws_config().await;
                cloudfront::invalidate_files(aws_cfg, distribution_id, files.iter())
                    .await
                    .unwrap();
            }
            // Cloudflare
            if let Some(zone_id) = &uploader_config.environments[&args.env].cloudflare_zone {
                let site_url = &uploader_config.environments[&args.env].url;
                let reqwest_client = reqwest_client_with_backoff();
                cloudflare::cloudflare_purge(&reqwest_client, zone_id, site_url, files.iter())
                    .await
                    .unwrap();
            }
        }
        Commands::SubmitURL { files } => {
            let sitemap_ignore_set = regex::RegexSet::new(
                uploader_config
                    .ignore_path
                    .iter()
                    .chain(uploader_config.sitemap.ignore_path.iter()),
            )
            .unwrap();

            let site_url = &uploader_config.environments[&args.env].url;
            let reqwest_client = reqwest_client_with_backoff();
            let html_changed: Vec<&Arc<Path>> = files
                .iter()
                .filter(|p| {
                    let ext = p.extension();
                    ext.is_some_and(|e| e == "html")
                })
                .filter(|p| {
                    let s = p.to_str();
                    s.is_some_and(|ss| !sitemap_ignore_set.is_match(ss))
                })
                .collect();

            bing_submit_url_batch(&reqwest_client, site_url, html_changed.into_iter())
                .await
                .unwrap();
        }
        Commands::Slack { files } => {
            let reqwest_client = reqwest_client_with_backoff();

            slack_webhook::notify(&reqwest_client, &Ok(files.to_vec()))
                .await
                .unwrap();
        }
        Commands::SetUploaded { uploaded } => {
            let aws_cfg = uploader_aws_config().await;
            let dynamodb_cfg = &uploader_config.dynamodb.as_ref().unwrap();
            let db_client = db::make_client(aws_cfg, &dynamodb_cfg.region).await;
            let key = uploader_config.environments[&args.env]
                .dynamodb_key
                .as_ref()
                .unwrap();
            db::put_commitid(&db_client, &dynamodb_cfg.table, key, uploaded)
                .await
                .unwrap();
        }
        Commands::GetUploaded => {
            let aws_cfg = uploader_aws_config().await;
            let dynamodb_cfg = &uploader_config.dynamodb.as_ref().unwrap();
            let db_client = db::make_client(aws_cfg, &dynamodb_cfg.region).await;
            let key = uploader_config.environments[&args.env]
                .dynamodb_key
                .as_ref()
                .unwrap();
            let res = db::get_commitid(&db_client, &dynamodb_cfg.table, key)
                .await
                .unwrap();
            println!("{}", res);
        }
    }
}
