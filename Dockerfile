FROM registry.suse.com/bci/rust:1.84.0-1.2.8 as builder

ARG jobs=default

WORKDIR /app

COPY Cargo.toml Cargo.lock ./
COPY src src

RUN cargo build --release -j $jobs --no-default-features --features rustls; \
	cp -ap target/release/uploader .

RUN for bin in /bin/bash /usr/bin/bash ; do \
		install -Dp $bin /targetroot$bin ;\
	done; \
	for lib in $(ldd /bin/bash | cut -d" " -f 3 | grep -E -v 'lib(c|m|dl|pthread)\.so'); do \
		install -Dp $lib /targetroot$lib ; \
	done; \
	install -Dp uploader /targetroot/usr/local/bin/uploader ; \
	for lib in $(ldd uploader | cut -d" " -f 3 | grep -E -v 'lib(c|m|dl|pthread)\.so'); do \
		install -Dp $lib /targetroot$lib ; \
	done


FROM registry.suse.com/bci/bci-busybox:15.7-3.22
COPY --from=builder /targetroot/ /
